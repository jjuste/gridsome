---
description: ''
sidebar: 'docs'
next: '/docs/renderingandtemplates/'
---

# Rendering and Templates

Empire is based on a client-side rendering system. All empire pages are simple HTML pages that contents are found in a large JSON object typically referred to as "Page JSON." A pages Page JSON is an extensive collection of objects and arrays that contain different types of metadata, configuration, and ASTs needed to render a page accordingly. 

Page JSON can be found in all pages by referencing the global `fwData` object in the browsers developer console. The root properties of the `fwData` are as follows:.


| Property/Key | Data Type | Platform | Description |
| ------------ | --------- | ----------- | ----------- |
| header | AST | Empire/eCore | Header AST |
| footer | AST | eCore | Footer AST |
| page | AST | Empire/eCore | Page AST |
| context | Object | Empire/eCore | Contains page, framework, and component level configurations and metadata. Example: session `keepAlive`, `ui` asset references, and `screen` information |
| menus | Object | Empire/eCore | Contains the main page level menus. Each menu is a directly named sub object like `global` or `system`. The menus the appear below this root object are depends on the platform.  |
| preferences | Object | Empire | Users saved platform specific configurations. Example. tabset pin setting, table preferences |
| notifications | Object | Empire | Platform notifications |

## What an AST is, how its used, what are the common properties

An AST or Abstract Syntax Tree is a collection of objects defining the overall structure of a page. Each node is built from a set of standard and node-specific/template properties. These nodes are paired with the correct corresponding templates, which will render the proper corresponding HTML component or node.

Below are the common AST properties used by the rendering component and how they are used by the render. 

| Property/Key | Data Type | Description |
| ------------ | --------- | ----------- |
| type | String | Type is a string used either to specify a specific mode a template should run in or the actual HTML element that should be rendered when the template property is not specified. |
| template | String | The specific template used to render the current object. When this property is missing, the render will set the property to `universal.` The universal template is a special template that uses an object `type` property as the literal node name for the DOM it would create. If the `type` property is also missing, the render will substitute the current object node with a standard `<div>` element.. |
| contents | Array | Contents define an object node as a branch in the overall structure of the DOM tree. When the render comes across any template node that contains this property, it will loop through the contents children array recursively appending any children results to the current object DOM object. This only happens if the existing object node can properly generate a valid DOM element. |
| attributes | Object | The contents should contain only key-value pair strings that are applied to the root object being rendered. |

## Fast by default

This is the catchphrase of Gridsome and true in any sense of the word. Static site generators output plain html files and have other great features like image processing and lazy-loading. After Serving the initial html, Gridsome site turn into a snappy single page application.

If I may quote Gridsome themselves:

> Gridsome builds ultra performance into every page automatically. You get code splitting, asset optimization, progressive images, and link prefetching out of the box. With Gridsome you get almost perfect page speed scores by default.

In combination with [Netlify](https://www.netlify.com/) this theme gives you a perfect Lighthouse score out of the box.

## Simple Navigation

Any good documentation has great navigation. This theme has support for an organized sidebar fore cross-page navigation as well as an autmatic generated table of contents for each page in your documentation.

## Search

The search component which is shipped with this theme, automatically indexes all headlines in your markdown pages and provides instant client side search powered by [Fuse.js](https://fusejs.io/).

## Dark Mode

This seems to be a must have for any site in current year. Click the icon at the top of the page and try it out for yourself!

## TailwindCSS

This starter uses [TailwindCSS](https://tailwindcss.com/) for layout and styling. You can easily configure it by editing the `tailwind.config.js` file. [PurgeCSS](https://purgecss.com/) is included as well to keep the bundle size as low as possible and the website fast and snappy!

### Changing Colors

The most inportant colors are defined in the `src/layouts/Default.vue` file at the top of the `style` block via CSS variables. If you want to change the primary color to orange for example, you would simply touch that value there.

```css
:rrot {
  --color-ui-primary: theme('colors.orange.600');
}
```

## Make it your own

Of course this is just a starter to quickly get you going. After downloading and installing you can do whatever you want with this theme. Check out the `src` folder and take a look at the components.

Docc uses [TailwindCSS](https://tailwindcss.com/). Colors and spacing can easily configured. To change the accent color, you only need to touch a single line in the code.

Don't like how something was designed or implemented? Just change the code and **make it your way**.

### Contribute

If you find any spelling mistakes or have improvements to offer, I am open to anyone who has ideas and wants to contribute to this starter theme.